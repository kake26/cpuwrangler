# Install.md 

1. Create dir /opt/scripts
2. Drop cpuwrangler.bash in the new directory
3. Edit file and change $cpugov to what you want to use, default to conservative
4. CD to /etc/systemd/system/
5. Drop in the cpuwrangler.service file
6. Run systemctl enable cpuwrangler.service

Thats it! 