# cpuwrangler

 CPU Wrangler for the PinePhone. 

 # What it does

 I recently got my PinePhone and noticed while there are many OSes out there, they seem to not pay attention to one thing. The CPU governor, leaving it on performance which is great for responsiveness, but eats battery. This script is a simple little tool to help adjust the governor. I've included a little bit of a description for each, but to get the full info you should look it up in the kernel docs. 

 # Supported Governors

 # Performance

 This is likely the default and just keeps the CPU at max power all the time. Downside is it drains battery power and generates allot of heat.

 # Ondemand

 A CPU governor that adjusts the power according to CPU load. A better balance between responsiveness and battery saving. Downside is its quick to
 jump to max power on the CPU. Still better then performance

 # Conservative

 Similar to the ondemand, but not as quick to ratchet up to max power on the CPU. This I think strikes a good balance between responsiveness and good
 battery savings. Especially, if the phone is mostly idle. 

 # Powersave

 Well, not much to say here except max power saving. 

 # Notes

 The service code for presisting the changes across the startup require systemd. However, using it elsewhere should be trivial.
