#!/bin/bash

# Some variables

sysdu="" # location to pull the systemd unit from github
cur_gov=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)
cpugov="conservative" # set what you want here

# First, are running as root?

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Default action is to reapply settings

echo $cpugov | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

